// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import AOS from "aos";
import "aos/dist/aos.css";

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'

import Vue from 'vue'
import App from './App.vue'

import vuetify from '@/plugins/vuetify' // path to vuetify export

import router from './router'
import './registerServiceWorker'





Vue.config.productionTip = false

Vue.use(VueSweetalert2);



new Vue({
  router,
  vuetify,
  created() {
    AOS.init({ disable: "phone" });
  },
  components: {
    App
  },
  render: h => h(App)
}).$mount('#app')
