onmessage = function(e) {
  // eslint-disable-next-line no-console
  console.log('Message received from main script');
  var workerResult = 'Result: ' + (e.data[0]);
  // eslint-disable-next-line no-console
  console.log('Posting message back to main script');
  postMessage(workerResult);
}
