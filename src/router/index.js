import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import faceScan from '../views/faceScan.vue'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  name: 'home',
  component: Home
}, {
  path: '/faceScan',
  name: 'faceScan',
  component: faceScan
}, ]

const router = new VueRouter({
  routes
})

export default router
